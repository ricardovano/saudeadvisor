﻿using System;
using System.Collections.Generic;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Front.Model
{
    public class Cidade
    {
        public List<Unidade> Unidades;
        public List<ComentarioDTO> Comentarios;
        public List<Unidade> MelhoresUnidades;
    }
}