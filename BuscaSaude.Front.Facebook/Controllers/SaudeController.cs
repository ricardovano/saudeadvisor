﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.WebPages;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Cidade = BuscaSaude.Front.Model.Cidade;

namespace BuscaSaude.Front.Controllers
{
    public class SaudeController : Controller
    {
        public static string URLSaude = "http://localhost:8090/api/Saude/";
        public static string URLRegiao = "http://localhost:8090/api/Regiao/";

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void EnviarAvaliacao(AvaliacaoDTO avaliacao)
        {
            //SetAvaliacaoByUnidadeId
            string url = URLSaude + "SetAvaliacaoByUnidadeId";
            using (var httpClient = new HttpClient())
            {
                using (var formData = new MultipartFormDataContent())
                {
                    //add content to form data
                    formData.Add(new StringContent(JsonConvert.SerializeObject(avaliacao)), "Content");

                    //add config to form data
                    //formData.Add(new StringContent(JsonConvert.SerializeObject(config)), "Config");

                    var response = httpClient.PostAsync(url, formData);
                    response.Wait();

                    if (!response.Result.IsSuccessStatusCode)
                    {
                        //error handling code goes here
                    }
                }
            }

            Unidade(avaliacao.UnidadeId);
        }

        public ActionResult Unidades(string pesquisa)
        {
            if (!String.IsNullOrEmpty(pesquisa))
            { 
                ViewBag.Pesquisa = pesquisa;
                pesquisa = pesquisa.ToUpper();
                        
                byte[] tempBytes;
                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(pesquisa);
                string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

                string url = URLSaude + "GetUnidadeByKey?nameKey=" + asciiStr;
                var client = new WebClient();
                client.Encoding = Encoding.UTF8;
                var content = client.DownloadString(url);

                JavaScriptSerializer ser = new JavaScriptSerializer();
                ser.MaxJsonLength = Int32.MaxValue;
                List<Unidade> unidades = ser.Deserialize<List<Unidade>>(content);
            
                return View(unidades);
            }
            return View();
        }

        public ActionResult Unidade(string pesquisa)
        {
            if (!String.IsNullOrEmpty(pesquisa))
            {
                string url = URLSaude + "GetUnidadeById?unidadeId=" + pesquisa;

                var client = new WebClient();
                client.Encoding = Encoding.UTF8;
                var content = client.DownloadString(url);

                JavaScriptSerializer ser = new JavaScriptSerializer();
                Unidade unidade = ser.Deserialize<Unidade>(content);
                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
                ViewBag.Pesquisa = pesquisa;

                string urlGetEstatistica = URLSaude +
                    "GetEstatisticaByUnidadeId?estatisticaUnidadeId=" +
                    unidade.Id;
                var clientEstatistica = new WebClient();
                var contentEstatistica = clientEstatistica.DownloadString(urlGetEstatistica);
                JavaScriptSerializer serEstatistica = new JavaScriptSerializer();
                Pontuacao pontuacao = serEstatistica.Deserialize<Pontuacao>(contentEstatistica);
                pontuacao.PontuacaoMedia = Convert.ToInt32(pontuacao.PontuacaoMedia);
                ViewBag.pontuacao = pontuacao;

                //string AppId = "WV1ejBrcW9Im7ym1Djii";
                //string AppCode = "hV9Ermv8lTXCi_bDRmWlZA";
                string CoordX = unidade.Endereco.Location.Coordinates[1].ToString().Replace(",",".");
                string CoordY = unidade.Endereco.Location.Coordinates[0].ToString().Replace(",", ".");

                if (unidade.Telefones == null)
                {
                    unidade.Telefones = new List<Telefone>();
                    unidade.Telefones.Add(new Telefone { DDD = "", Numero = "" });
                }
                if (unidade.Endereco == null)
                {
                    unidade.Endereco = new Endereco();
                }

                string imageMap =
                    "https://image.maps.cit.api.here.com/mia/1.6/mapview?c="+ CoordX + "%2C" + CoordY + "&z=14&app_id=WV1ejBrcW9Im7ym1Djii&app_code=hV9Ermv8lTXCi_bDRmWlZA";
                ViewBag.ImageMap = imageMap;

                return View(unidade);
            }

            return View();
 
        }

        public ActionResult Cidade(string pesquisa)
        {
            ViewBag.Pesquisa = pesquisa;
            pesquisa = pesquisa.ToUpper();

            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(pesquisa);
            string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

            string url = URLRegiao + "GetRegiaoByName?name=" + asciiStr;
            var client = new WebClient();
            client.Encoding = Encoding.UTF8;
            var content = client.DownloadString(url);

            JavaScriptSerializer ser = new JavaScriptSerializer();
            List<Unidade> unidades = ser.Deserialize<List<Unidade>>(content);
            
            var comentarios = new List<ComentarioDTO>();
            foreach (var unidade in unidades)
            {
                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
                else
                {
                    foreach (var a in unidade.Avaliacoes)
                    {
                        ComentarioDTO c = new ComentarioDTO();
                        c.Comentario = a.Descricao;
                        c.Data = a.Data;
                        c.Nome = a.Name;
                        c.NotaRecomenda = a.NotaRecomenda;
                        c.Anonimo = a.Anonimo;
                        c.Unidade = unidade.Name;
                        c.UnidadeId = unidade.Id;
                        comentarios.Add(c);
                    }
                }

            }

            Model.Cidade cidade = new Cidade();
            cidade.Unidades = new List<Unidade>();
            if (unidades.Count > 3)
                cidade.MelhoresUnidades = unidades.OrderByDescending(m => m.Score).ToList().GetRange(0, 3);
            
            cidade.Unidades = unidades;
            cidade.Comentarios = comentarios;

            return View(cidade);
        }
    }

    public class Regiao
    {
        public String City;
        public String UF;
        public List<Unidade> Unidades;
        public List<Unidade> BestList;
        public List<ComentarioDTO> CommentsList;
    }

    public class ClientPontuacao
    {
        public int Infraestrutura { get; set; }
        public int Atendimento { get; set; }
        public int LimpezaHigiene { get; set; }
        public int Recomendacoes { get; set; }
        public int PontuacaoMedia { get; set; }

        public string notasHLA { get; set; }
        public string notasHLB { get; set; }
        public string notasHLC { get; set; }
        public string notasHLD { get; set; }
        public string notasHLE { get; set; }

        public string notasATA { get; set; }
        public string notasATB { get; set; }
        public string notasATC { get; set; }
        public string notasATD { get; set; }
        public string notasATE { get; set; }

        public string notasESA { get; set; }
        public string notasESB { get; set; }
        public string notasESC { get; set; }
        public string notasESD { get; set; }
        public string notasESE { get; set; }

        public string notasOGA { get; set; }
        public string notasOGB { get; set; }
        public string notasOGC { get; set; }
        public string notasOGD { get; set; }
        public string notasOGE { get; set; }

        public string MediaStarA { get; set; }
        public string MediaStarB { get; set; }
        public string MediaStarC { get; set; }
        public string MediaStarD { get; set; }
        public string MediaStarE { get; set; }
    }
}