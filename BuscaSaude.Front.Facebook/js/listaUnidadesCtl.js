app.controller('listaUnidadesCtl', function ($scope, $http) {
    $scope.name = gup("pesquisa").replace(/%20/g, ' ');
    $scope.loading = true;

    $http.get(getUrl('Saude') + '/GetUnidadeBySearch?searchString=' + $scope.name )
    .success(function(data) {
        $scope.unidades = data;
        $scope.loading = false;
    })
    .error(function(data) {
        console.log('Erro: ' + data);
        $scope.loading = false;
    });

    $scope.getDetail = function(name){
        return name.replace(/ /g, '-');
    }

    $scope.getColor = function(index){
        if(index % 2 == 0)
            return '#f3f3f3';

        return '#fff';
    }


});