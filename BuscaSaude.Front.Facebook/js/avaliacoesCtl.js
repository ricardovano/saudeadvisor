app.controller('avaliacoesCtl', function ($scope, $http) {
    var s = gup('s');

    if (s == 'r'){
    $scope.unidades = [];
    $scope.loading = true;

    $http.get(getUrl('Site') + '/GetLastAvaliacoes')
        .success(function(data) {
            $scope.unidades = data;
            $scope.loading = false;
        })
        .error(function(data) {
            console.log('Erro: ' + data);
            $scope.loading = false;
        });

    }
});
