﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
namespace BuscaSaude.Engine.Data
{
    public class MunicipioRepository : SimpleRepository<Model.Map.Municipio>
    {
        public new List<Model.Map.Municipio> GetAll()
        {
            return BaseCollection.FindAll().OrderBy(t => t.Nome).ToList();
        }

        public List<Model.Map.Municipio> GetMunicipioByName(string name)
        {
            var coll = BaseCollection.Find(Query<Model.Map.Municipio>.EQ(u => u.Nome, new BsonRegularExpression(new Regex(name, RegexOptions.IgnoreCase))));
            return coll.ToList();
        }
    }
}