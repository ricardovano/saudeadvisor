﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuscaSaude.Engine.Model;
using MongoDB.Driver.Builders;

namespace BuscaSaude.Engine.Data
{
    public class ContactRepository : SimpleRepository<Contact>
    {
        public List<Contact> GetOnlyNotRead()
        {
            var coll = BaseCollection.Find(Query<Contact>.Where(u => u.Lido == false));
            return coll.ToList();
        }
    }
}
