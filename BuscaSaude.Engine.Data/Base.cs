﻿using System.Configuration;
using MongoDB.Driver;

namespace BuscaSaude.Engine.Data
{
    public class Base
    {
        protected MongoClient client;
        protected MongoServer server;
        protected MongoDatabase database;

        public Base()
        {
            string connectionString = ConfigurationManager.AppSettings["MongoDBConnectionString"];
            string dbName = ConfigurationManager.AppSettings["MongoDBDatabase"];
            client = new MongoClient(connectionString);
            server = client.GetServer();
            database = server.GetDatabase(dbName);
        }

    }
}
