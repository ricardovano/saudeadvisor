﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace BuscaSaude.Engine.Data
{
    public class UnidadeRepository : SimpleRepository<Unidade>
    {
        public List<Unidade> GetUnidadesByRegiao(RegionDTO region)
        {
            var coll = BaseCollection.Find(Query<Unidade>.Matches(u => u.Endereco.Cidade.Name, new BsonRegularExpression(new Regex(region.Region,RegexOptions.IgnoreCase))));
            if (region.Skip > 0)
                coll.Skip = region.Skip;
            if(region.Limit > 0)
                coll.Limit = region.Limit;
            return coll.ToList();
        }

        public List<Unidade> GetUnidadesByRegiao(string name)
        {
            var coll = BaseCollection.Find(Query<Unidade>.EQ(u => u.Endereco.Cidade.KeyName, new BsonRegularExpression(new Regex(name, RegexOptions.IgnoreCase))));

            var lista = coll.ToList().OrderByDescending(u => u.UpdateDate).ToList();
            if (lista.Count > 50)
            {
                lista = lista.GetRange(0, 50);
            }
            return lista;
        }

        public List<Unidade> GetUnidadesByAvaliacao(string cidade)
        {
            return BaseCollection.Find(
                Query.And(
                    Query<Unidade>.EQ(u => u.Endereco.Name, cidade),
                    Query<Unidade>.LTE(u => u.Score, 0)
                )).OrderByDescending(s => s.Score).ToList();
        }

        public List<Unidade> GetUnidadesByCEP(string cep)
        {
            return BaseCollection.Find(
                Query.And(
                    Query<Unidade>.EQ(u => u.Endereco.CEP, cep)
                )).OrderByDescending(s => s.Score).ToList();
        }

        public new List<Unidade> GetAll()
        {
            return BaseCollection.FindAll().OrderBy(t => t.Name).ToList();
        }

        public List<Unidade> GetLastAvaliacoesByRegiao(string cidade)
        {
            var unidadesAvaliadas = BaseCollection.Find(
                Query.And(
                    Query<Unidade>.Matches(u => u.Endereco.Cidade.Name, new BsonRegularExpression(new Regex(cidade,RegexOptions.IgnoreCase))),
                    Query<Unidade>.GTE(u => u.Score, 0))
                    ).OrderByDescending(s => s.UpdateDate);

            return unidadesAvaliadas.ToList();
        }

        public List<Unidade> GetLastAvaliacoes()
        {
            var unidadesAvaliadas = BaseCollection.Find(
                Query.And(
                    Query<Unidade>.GTE(u => u.Score, 0)
                    )).Where(m => m.Avaliacoes != null).OrderByDescending(s => s.Score);

            return unidadesAvaliadas.ToList();
        }

        public Unidade GetUnidadesById(string id)
        {
            return BaseCollection.FindOne(Query<Unidade>.EQ(u => u.Id, id));
        }

        public IList<Unidade> GetByKeyList(string name, int max)
        {
            name = name.ToUpper();
            name = name.Replace(" EM ", " ");
            name = name.Replace(" NO ", " ");
            name = name.Replace(" NA ", " ");

            var names = name.Split();

            var query =
                from c in BaseCollection.AsQueryable<Unidade>()
                where c.Keys.ContainsAll(names)
                select c;

            var lista = query.ToList().OrderByDescending(u => u.UpdateDate).ToList();
            if (lista.Count > max)
            {
                lista = lista.GetRange(0, max);
            }
            return lista;
        }

        public IList<Unidade> GetByKeyList(string name)
        {
            name = name.ToUpper();
            name = name.Replace(" EM ", " ");
            name = name.Replace(" NO ", " ");
            name = name.Replace(" NA ", " ");

            var names = name.Split();

            var query =
                from c in BaseCollection.AsQueryable<Unidade>()
                where c.Keys.ContainsAll(names)
                select c;

            var lista = query.ToList().OrderByDescending(u => u.UpdateDate).ToList();
            if (lista.Count > 50)
            {
                lista = lista.GetRange(0, 50);
            }

            return lista;
        }

        public Int64 GetTotalByRegion(string region)
        {
            var count = BaseCollection.Find(Query<Unidade>.Matches(u => u.Endereco.Cidade.Name, new BsonRegularExpression(new Regex(region, RegexOptions.IgnoreCase)))).Count();
            return count;
        }
    }
}
