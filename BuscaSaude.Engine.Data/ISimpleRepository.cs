using System;
using System.Collections.Generic;
using BuscaSaude.Engine.Model;

namespace BuscaSaude.Engine.Data
{
    public interface ISimpleRepository<T> where T : ISimpleModel
    {
        T GetById(string id);
        T GetByName(string name);
        IList<T> GetBySearch(string search);
        T Create(T t);
        void Delete(T t);
        void Update(T t);
        T FindOne(string fieldName, object fieldValue);
        T FindOne(IDictionary<string, object> fieldValues);
        IList<T> FindAll(string fieldName, object fieldValue);
        IList<T> GetAll();
        long Count();
        void CleanUp();
        T UpdateSpecificAttribute(string Id, string attributeName, object attributeValue);
        void ForEach(Action<T> itemAction);
        void AddToArray(string id, string attributeName, object attributeValue);
        IList<string> GetAllIds();
        void CreateOrUpdate(T t);
        T GetByPropertyName(string propertyName, object propertyValue);
    }
}