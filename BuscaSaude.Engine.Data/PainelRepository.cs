﻿using BuscaSaude.Engine.Model.Painel;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.Linq;
using System;

namespace BuscaSaude.Engine.Data
{
    public class PainelRepository: SimpleRepository<Usuario>
    {
        public Usuario GetUserByCredential(Model.Painel.Usuario usuario)
        {
            var query =
                from c in BaseCollection.AsQueryable<Usuario>()
                where c.UserName == usuario.UserName && c.Password == usuario.Password
                select c;

            return query.SingleOrDefault();
        }

        public Usuario GetUserByToken(Model.Painel.Usuario usuario)
        {
            var query =
                from c in BaseCollection.AsQueryable<Usuario>()
                where c.Token == usuario.Token
                select c;

            return query.Single();
        }

        public int GetUserExist(Model.Painel.Usuario usuario)
        {
            var query =
                from c in BaseCollection.AsQueryable<Usuario>()
                where c.UserName == usuario.UserName
                select c;

            return query.Count();
        }
    }
}
