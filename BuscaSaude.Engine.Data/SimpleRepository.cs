﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuscaSaude.Engine.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace BuscaSaude.Engine.Data
{
    public class SimpleRepository<T> : Base, ISimpleRepository<T> where T : ISimpleModel
    {
        protected virtual string GetCollectionName()
        {
            var collectionName = typeof(T).Name + "s";
            collectionName = collectionName.Replace("ys", "ies");
            if (collectionName.EndsWith("chs"))
            {
                collectionName = collectionName.Substring(0, collectionName.Length - 3) + "ches";
            }
            return collectionName;
        }

        protected MongoCollection<T> BaseCollection
        {
            get
            {
                {
                    var collectionName = GetCollectionName();
                    return database.GetCollection<T>(collectionName);
                }
            }
        }

        public T GetById(string id)
        {
            return BaseCollection.FindOne(Query<T>.EQ(u => u.Id, id));
        }

        public T GetByName(string name)
        {
            name = name.ToUpper();
            var un = BaseCollection.FindOne(Query<T>.EQ(u => u.Name, name));
            return un;
        }

        public IList<T> GetBySearch(string search)
        {
            search = search.ToUpper();
            //var coll = BaseCollection.Find(Query<T>.Matches(u => u.Name, new BsonRegularExpression("/." + name + "./i")) );
            var coll = BaseCollection.Find(Query<T>.Matches(u => u.Name, search));
            coll.Limit = 50;
            coll.OrderBy(u => u.Name);
            return coll.ToList();
        }

        protected virtual void InitializeFields(T t)
        {

        }

        public virtual T Create(T t)
        {
            InitializeFields(t);
            t.Id = CombGenerator.GenerateComb().ToString();
            BaseCollection.Insert(t);
            return t;
        }

        public void Delete(T t)
        {
            BaseCollection.Remove(Query<T>.EQ(u => u.Id, t.Id));
        }

        public void Update(T t)
        {
            BaseCollection.Save(t);
        }

        public T FindOne(string fieldName, object fieldValue)
        {
            return BaseCollection.FindOne(Query.EQ(fieldName, BsonValue.Create(fieldValue)));
        }

        public T FindOne(IDictionary<string, object> fieldValues)
        {
            return BaseCollection.FindOne(new QueryDocument(fieldValues));
        }

        public IList<T> FindAll(string fieldName, object fieldValue)
        {
            return BaseCollection.Find(Query.EQ(fieldName, BsonValue.Create(fieldValue))).ToList();
        }

        public IList<T> GetAll()
        {
            return BaseCollection.FindAll().OrderBy(t => t.Name).ToList();
        }

        public long Count()
        {
            return BaseCollection.Count();
        }

        public void CleanUp()
        {
            BaseCollection.RemoveAll();
        }


        public void AddToArray(string id, string attributeName, object attributeValue)
        {
            UpdateBuilder updateBuilder = new UpdateBuilder();
            updateBuilder.PushWrapped(attributeName, attributeValue);
            BaseCollection.Update(Query<T>.EQ(a => a.Id, id), updateBuilder);
        }


        public T UpdateSpecificAttribute(string id, string attributeName, object attributeValue)
        {
            UpdateBuilder ub = new UpdateBuilder();
            if (attributeValue == null)
                ub.Set(attributeName, BsonNull.Value);
            else
                ub.Set(attributeName, BsonValue.Create(attributeValue));
            BaseCollection.Update(Query<T>.EQ(a => a.Id, id), ub);
            return BaseCollection.FindOneById(id);
        }

        public IList<string> GetAllIds()
        {
            IList<string> result = new List<string>();
            ForEach((a) => result.Add(a.Id));
            return result;
        }

        public void CreateOrUpdate(T t)
        {
            BaseCollection.Save(t);
        }

        public T GetByPropertyName(string propertyName, object propertyValue)
        {
            return BaseCollection.FindOne(Query.EQ(propertyName, BsonValue.Create(propertyValue)));
        }

        public void ForEach(Action<T> itemAction)
        {
            MongoCursor<T> cursor = BaseCollection.FindAll().SetFlags(QueryFlags.NoCursorTimeout).SetSnapshot();

            try
            {
                foreach (T item in cursor)
                {
                    itemAction(item);
                }
            }
            finally
            {
                //exhaust
                cursor.Last();
            }
        }


    }
}
