﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuscaSaude.Engine.Data;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.Core
{
    public class PainelService
    {
        private PainelRepository painelRepository;
        public PainelService()
        {
            painelRepository = new PainelRepository();
        }

        public Model.Painel.Usuario GetUserByCredential(Model.Painel.Usuario usuario)
        {
            usuario.SetHashValue();
            var retorno = painelRepository.GetUserByCredential(usuario);
            return retorno;
        }

        public Model.Painel.Usuario GetUserById(string id)
        {
            var retorno = painelRepository.GetById(id);
            return retorno;
        }

        public Model.Painel.Usuario GetUserByToken(Model.Painel.Usuario usuario)
        {
            usuario.SetHashValue();
            var retorno = painelRepository.GetUserByToken(usuario);
            return retorno;
        }

        public Model.Painel.Usuario PostCadastro(Model.Painel.Usuario usuario)
        {
            usuario.SetHashValue();                       
            var existe = painelRepository.GetUserExist(usuario);
            if (existe < 1)
            {
                usuario.StatusAtual = Model.Painel.Usuario.Status.Ativo;
                usuario = painelRepository.Create(usuario);
            }
            else
                usuario = null;

            return usuario;
        }
    }
}