﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuscaSaude.Engine.Data;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.Core
{
    public class UnidadeService
    {
        private UnidadeRepository unidadeRepository;
        private ContactRepository contactRepository; 
        public UnidadeService()
        {
            unidadeRepository = new UnidadeRepository();
            contactRepository = new ContactRepository();
        }

        public void Insert(Model.Unidade unidade)
        {
            var unidadeBase = unidadeRepository.GetByName(unidade.Name);
            if (unidadeBase != null && unidadeBase.Name == unidade.Name)
            {
                //unidade.Id = unidadeBase.Id;
                //unidadeRepository.Update(unidade);
            }
            else
            {
                unidadeRepository.Create(unidade);    
            }            
        }

        public void Update(Model.Unidade unidade)
        {
            unidadeRepository.Update(unidade);
        }

        public void Delete(Model.Unidade unidade)
        {
            unidadeRepository.Delete(unidade);
        }

        public List<Unidade> GetAll()
        {
            var unidades = unidadeRepository.GetAll().ToList();
            return unidades;
        }

        public Unidade GetUnidadeByName(string name)
        {          
            var unidade = unidadeRepository.GetByName(name);
            return (Unidade)unidade;
        }

        public List<Unidade> GetUnidadesByCEP(string CEP)
        {
            var unidades = unidadeRepository.GetUnidadesByCEP(CEP);
            return unidades;
        }

        public List<Unidade> GetUnidadeBySearchString(string search)
        {
            try
            {
                var unidades = unidadeRepository.GetBySearch(search);
                return (List<Unidade>) unidades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Unidade> GetUnidadesByAvaliacao(string regiao)
        {
            var unidades = unidadeRepository.GetUnidadesByAvaliacao(regiao);
            return unidades;
        }

        public Unidade GetUnidadeById(string id)
        {
            var unidade = unidadeRepository.GetUnidadesById(id);
            return unidade;
        }

        public Pontuacao GetEstatisticaByUnidadeId(string estatisticaUnidadeId)
        {
            var unidade = unidadeRepository.GetUnidadesById(estatisticaUnidadeId);
            var pontuacao = new Pontuacao();
            unidade.Score = 0;
            if (unidade.Avaliacoes != null)
            {
                double total = unidade.Avaliacoes.Count();
                double totalEstrutura = unidade.Avaliacoes.Sum(m => m.NotaEstrutura);
                double totalAtendimento = unidade.Avaliacoes.Sum(m => m.NotaAtendimento);
                double totalLimpeza = unidade.Avaliacoes.Sum(m => m.NotaHigieneLimpeza);
                double totalRemomenda = unidade.Avaliacoes.Sum(m => m.NotaRecomenda);

                double scoreEstrutura = (totalEstrutura / (total * 100)) * 100;
                double scoreAtendimento = (totalAtendimento / (total * 100)) * 100;
                double scoreLimpeza = (totalLimpeza / (total * 100)) * 100;
                double scoreRemomenda = (totalRemomenda / (total * 100)) * 100;

                double score = ((scoreEstrutura + scoreAtendimento + scoreLimpeza + scoreRemomenda)/400)*100;
                
                pontuacao.Atendimento = scoreAtendimento;
                pontuacao.Infraestrutura = scoreEstrutura;
                pontuacao.LimpezaHigiene = scoreLimpeza;
                pontuacao.Recomendacoes = scoreRemomenda;
                pontuacao.PontuacaoMedia = score;
                unidade.Score = score;
                unidade.UpdateDate = DateTime.Now;
                unidadeRepository.Update(unidade);
            }            
            return pontuacao;
        }

        public void SetAvaliacaoByUnidadeId(AvaliacaoDTO avaliacao)
        {
            var unidade = unidadeRepository.GetUnidadesById(avaliacao.UnidadeId);
            if (unidade.Avaliacoes == null)
                unidade.Avaliacoes = new List<Avaliacao>();
            avaliacao.Dados.Email = avaliacao.Dados.Email.ToLower();
            avaliacao.Dados.Data = DateTime.Now;
            unidade.Avaliacoes.Add(avaliacao.Dados);
            unidadeRepository.Update(unidade);
        }

        public void SendContact(Contact contact)
        {
            contactRepository.Create(contact);
        }

        public List<Unidade> GetUltimasAvaliacoes()
        {
            var unidades = unidadeRepository.GetLastAvaliacoes();
            return unidades;
        }

        public List<Unidade> GetUnidadeByKey(string name)
        {
            var unidades = unidadeRepository.GetByKeyList(name);
            return unidades.ToList();
        }

        public List<Unidade> GetUnidadeByKey(string name, int max)
        {
            var unidades = unidadeRepository.GetByKeyList(name, max);
            return unidades.ToList();
        }

        public List<Contact> GetMensagens(bool incluirLidos)
        {
            if (incluirLidos)
            {
                return contactRepository.GetAll().ToList();
            }
            return contactRepository.GetOnlyNotRead();
        }

        public List<Unidade> GetRegionByName(RegionDTO region)
        {
            var unidades = unidadeRepository.GetUnidadesByRegiao(region);
            foreach (var unidade in unidades)
            {
                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
            }
            var ordenado = unidades.OrderByDescending(s => s.Avaliacoes.Count);
            return ordenado.ToList();
        }

        public List<Unidade> GetRegionByName(string name)
        {
            name = name.ToUpper();

            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(name);
            string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

            var unidades = unidadeRepository.GetUnidadesByRegiao(asciiStr);
            foreach (var unidade in unidades)
            {
                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
            }
            var ordenado = unidades.OrderByDescending(s => s.Avaliacoes.Count);
            return ordenado.ToList();
        }

        public List<Unidade> GetBestByRegion(string local)
        {
            RegionDTO r = new RegionDTO();
            r.Region = local;
            r.Limit = -1;
            r.Skip = -1;
            var unidades = unidadeRepository.GetUnidadesByRegiao(r);

            if (unidades.Count > 3)
                return unidades.OrderByDescending(m => m.Score).ToList().GetRange(0, 3);

            return unidades;
        }

        public List<ComentarioDTO> GetLastComentariosByRegion(string region)
        {
            var unidades = unidadeRepository.GetLastAvaliacoesByRegiao(region);
            var comentarios = new List<ComentarioDTO>();
            foreach (var unidade in unidades)
            {
                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
                else
                {
                    foreach (var a in unidade.Avaliacoes)
                    {
                        ComentarioDTO c = new ComentarioDTO();
                        c.Comentario = a.Descricao;
                        c.Data = a.Data;
                        c.Nome = a.Name;
                        c.NotaRecomenda = a.NotaRecomenda;
                        c.Anonimo = a.Anonimo;
                        c.Unidade = unidade.Name;
                        c.UnidadeId = unidade.Id;
                        comentarios.Add(c);
                    }                    
                }
                    
            }
            return comentarios.OrderByDescending(m => m.Data).ToList();
        }

        public Int64 GetTotalByRegion(string region)
        {
            return unidadeRepository.GetTotalByRegion(region);
        }
    }
}
