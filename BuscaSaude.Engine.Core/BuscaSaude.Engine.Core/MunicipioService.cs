﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuscaSaude.Engine.Data;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.Core
{
    public class MunicipioService
    {
        private MunicipioRepository municipioRepository;

        public MunicipioService()
        {
            municipioRepository = new MunicipioRepository();

        }

        public void Insert(Model.Map.Municipio municipio)
        {
            var unidadeBase = municipioRepository.GetByName(municipio.Nome);
            if (unidadeBase != null && unidadeBase.Name == municipio.Nome)
            {
                //unidade.Id = unidadeBase.Id;
                //unidadeRepository.Update(unidade);
            }
            else
            {
                municipioRepository.Create(municipio);
            }
        }

        public List<Model.Map.Municipio> GetAll()
        {
            var municipios = municipioRepository.GetAll().ToList();
            return municipios;
        }
    }
}
