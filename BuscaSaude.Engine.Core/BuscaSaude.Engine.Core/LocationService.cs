﻿using System;
using System.Configuration;
using System.Net;
using System.Text;
using BuscaSaude.Engine.Model;
using Newtonsoft.Json.Linq;

namespace BuscaSaude.Engine.Core
{
    public class LocationService
    {
        public Endereco GeocodeAddress(Endereco address)
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = UTF8Encoding.UTF8;
                string addressString = "";
                try
                {
                    addressString = address.ToBaseString();
                    string appid = ConfigurationManager.AppSettings["NokiaAppId"];
                    string appcode = ConfigurationManager.AppSettings["NokiaAppCode"];

                    string format = string.Format(
                        "https://geocoder.cit.api.here.com/6.2/geocode.json?housenumber={0}&street={1}&city={2}&country=brasil&gen=8&app_id={3}&app_code={4}",
                        address.Numero, address.Logradouro, address.Cidade.Name, appid, appcode);

                    var content = wc.DownloadString(format);
                    var response = JToken.Parse(content);
                    double[] longLat = new double[2];
                    try
                    {
                        GetLongLat(longLat, response);
                        address.Cidade.Estado = new Estado();
                        address.Cidade.Estado.UF = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["State"].Value<string>();
                        address.Cidade.Name = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["City"].Value<string>();
                        if(response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["District"] != null)
                            address.Bairro = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["District"].Value<string>();

                        address.Logradouro = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["Street"].Value<string>();
                        address.CEP = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["PostalCode"].Value<string>();
                    }
                    catch
                    {
                        try
                        {
                            content = wc.DownloadString(string.Format(
                                "https://geocoder.cit.api.here.com/6.2/geocode.json?searchtext={0}&app_id={1}&app_code={2}",
                                addressString, appid, appcode));
                            response = JToken.Parse(content);
                            longLat = new double[2];
                            GetLongLat(longLat,response);
                            address.Cidade.Estado = new Estado();
                            address.Cidade.Estado.UF = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["State"].Value<string>();
                            if (response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["District"]!=null)
                                address.Bairro = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["District"].Value<string>();
                            address.Logradouro = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["Street"].Value<string>();
                            address.CEP = response["Response"]["View"][0]["Result"][0]["Location"]["Address"]["PostalCode"].Value<string>();
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                addressString = "Brasil - " + address.Logradouro;
                                content = wc.DownloadString(string.Format(
                                    "https://geocoder.cit.api.here.com/6.2/geocode.json?searchtext={0}&app_id={1}&app_code={2}",
                                    addressString, appid, appcode));
                                response = JToken.Parse(content);
                                longLat = new double[2];
                                GetLongLat(longLat, response);
                            }
                            catch
                            {
                                //NOTHING!! BOLA PRA FRENTE!!
                            }

                        }
                    }

                    var location = new Location
                    {
                        Type = "Point",
                        Coordinates = longLat
                    };
                    address.Location = location;
                    return address;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
        }

        private static void GetLongLat(double[] longLat, JToken response)
        {
            longLat[0] = response["Response"]["View"][0]["Result"][0]["Location"]["DisplayPosition"]["Longitude"].Value<double>();
            longLat[1] = response["Response"]["View"][0]["Result"][0]["Location"]["DisplayPosition"]["Latitude"].Value<double>();
        }

    }
}
