﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class User : SimpleModel
    {
        //Login
        public string Email { get; set; }
        public TipoAutenticador Autenticador  { get; set; }
        public string Token { get; set; }
    }

    public enum TipoAutenticador
    {
        Google,
        Facebook,
        Email
    }
}
