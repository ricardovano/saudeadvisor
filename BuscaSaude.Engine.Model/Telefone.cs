﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Telefone : SimpleModel
    {
        public string DDD { get; set; }
        public string Numero { get; set; }
        public string Ramal { get; set; }
    }
}
