namespace BuscaSaude.Engine.Model
{
    public interface ISimpleModel : IModelRoot
    {
        string Name { get; set; }
    }
}