﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Convenio
    {
        public string Nome { get; set; }
        public string Plano { get; set; }
        public string Produto { get; set; }
    }
}
