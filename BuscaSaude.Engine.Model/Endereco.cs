﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Endereco : SimpleModel
    {        
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public Cidade Cidade { get; set; }
        public Location Location { get; set; }

        public string ToBaseString()
        {
            return Logradouro + ", " + Numero + ", " + Cidade.Name + ", Brasil";
        }
    }
    public class Location
    {
        public string Type { get; set; }
        public double[] Coordinates { get; set; }
    }
}
