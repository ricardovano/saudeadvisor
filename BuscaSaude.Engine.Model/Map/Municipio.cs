﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model.Map
{
    public class Municipio :  SimpleModel
    {
        public string Nome { get; set; }
        public string UF { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }

    }
}
