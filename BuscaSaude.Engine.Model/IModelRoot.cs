namespace BuscaSaude.Engine.Model
{
    public interface IModelRoot
    {
        string Id { get; set; }
    }
}