﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class SimpleModel : ISimpleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
