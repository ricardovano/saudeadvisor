﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model.Painel
{
    public class Usuario : SimpleModel
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NumeroDocumento { get; set; }
        public TipoDocumento Documento { get; set; }
        public PerfilUsuario Perfil { get; set; }
        public Status StatusAtual { get; set; }

        public enum PerfilUsuario { Cliente, Usuario, Administrador, Parceiro };
        public enum TipoDocumento {  CPF, CNPJ };
        public enum Status {  SenhaInvalida, Novo, Ativo, Suspenso}

        public void SetHashValue()
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes;
            using (HashAlgorithm hash = SHA512.Create())
                hashBytes = hash.ComputeHash(encoding.GetBytes(this.Password));

            StringBuilder hashValue = new StringBuilder(hashBytes.Length * 2);
            foreach(byte b in hashBytes)
            {
                hashValue.AppendFormat(CultureInfo.InvariantCulture, "{0:X2}", b);
            }
            this.Password = hashValue.ToString();
        }   
    }
}
