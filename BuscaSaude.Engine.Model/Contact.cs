﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Contact : SimpleModel
    {
        public string Descricao { get; set; }
        public string Email { get; set; }
        public bool Lido { get; set; }

        public string Telefone { get; set; }
        public string CentroMedico { get; set; }

        public string ContactName { get; set; }
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }

    }
}
