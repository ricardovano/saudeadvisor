﻿using System;
using System.Collections.Generic;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.Model.DTO
{
    public class CidadeDTO
    {
        public List<Unidade> Unidades;
        public List<ComentarioDTO> Comentarios;
        public List<Unidade> MelhoresUnidades;
    }
}