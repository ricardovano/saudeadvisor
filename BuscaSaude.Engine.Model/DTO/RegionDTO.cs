﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model.DTO
{
    public class RegionDTO
    {
        public string Region { get; set; }
        public int Limit { get; set; }
        public int Skip { get; set; }
    }
}
