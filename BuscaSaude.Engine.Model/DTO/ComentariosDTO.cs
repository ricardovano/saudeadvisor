﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model.DTO
{
    public class ComentarioDTO
    {
        public string Unidade { get; set; }
        public string UnidadeId { get; set; }
        public string Comentario { get; set; }
        public DateTime Data { get; set; }
        public string Nome { get; set; }
        public double NotaRecomenda { get; set; }
        public bool Anonimo { get; set; }
    }
}
