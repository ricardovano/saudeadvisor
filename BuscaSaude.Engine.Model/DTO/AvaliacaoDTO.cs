﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model.DTO
{
    public class AvaliacaoDTO
    {
        public Avaliacao Dados { get; set; }
        public string UnidadeId { get; set; }
    }
}
