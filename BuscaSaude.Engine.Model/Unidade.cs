﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Unidade : SimpleModel
    {
        public Int32 IdBase { get; set; } 
        public string CNES { get; set; }
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
        public EsferaAdministrativa EsferaAdministrativa { get; set; }
        public Gestao Gestao { get; set; }  
        public TipoEstabelecimento TiposEstabelecimento { get; set; }
        public Endereco Endereco { get; set; }
        public List<Telefone> Telefones { get; set; }
        public double Score { get; set; }
        public List<Avaliacao> Avaliacoes { get; set; }
        public string[] Keys { get; set; }

        public DateTime UpdateDate { get; set; }
        public string HorarioAtendimento { get; set; }
        public List<Convenio> Convenios { get; set; }
        public List<string> Especialidades  { get; set; }
        public List<string> Artigos  { get; set; }
        public List<Medico> Medicos { get; set; } 
    }

}
