﻿using System.Collections.Generic;

namespace BuscaSaude.Engine.Model.Especialidades
{
    public class Especialidade
    {
        public string Nome { get; set; }
        public List<string> Sinonimos { get; set; }
    }
}
