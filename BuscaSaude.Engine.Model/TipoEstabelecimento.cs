﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class TipoEstabelecimento : SimpleModel
    {
        public string Descricao { get; set; }
    }
}
