﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Medico
    {
        public string CRM { get; set; }
        public string Nome { get; set; }
        public string Especialidade { get; set; }
    }
}
