﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Avaliacao : SimpleModel
    {
        public string Email { get; set; }        
        public string Descricao { get; set; }
        public bool Recomenda { get; set; }
        public bool Atendimento { get; set; }
        public bool HigieneLimpeza { get; set; }
        public bool Estrutura { get; set; }

        public double NotaRecomenda { get; set; }
        public double NotaAtendimento { get; set; }
        public double NotaHigieneLimpeza { get; set; }
        public double NotaEstrutura { get; set; }

        public DateTime Data { get; set; }

        public bool Anonimo { get; set; }
    }

    public class Pontuacao : SimpleModel
    {
        public double Infraestrutura { get; set; }
        public double Atendimento { get; set; }
        public double LimpezaHigiene { get; set; }
        public double Recomendacoes { get; set; }
        public double PontuacaoMedia { get; set; }

    }
}
