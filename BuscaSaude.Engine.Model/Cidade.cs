﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaSaude.Engine.Model
{
    public class Cidade : SimpleModel
    {
        public Estado Estado { get; set; }
        public string KeyName { get; set; }
    }
}
