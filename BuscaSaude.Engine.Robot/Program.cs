﻿using System;
using System.Collections.Generic;
using System.Net;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model;
using System.IO;
using System.Globalization;

namespace BuscaSaude.Engine.Robot
{
    class Program
    {
        public static List<Model.Unidade> Unidades = new List<Model.Unidade>();
        public static List<Model.Map.Municipio> Cidades = new List<Model.Map.Municipio>();
        public static List<Model.Especialidades.Especialidade> Especializacoes = new List<Model.Especialidades.Especialidade>();

        static void Main(string[] args)
        {
            //CarregaDicionario();
            //AtualizaDicionario();
           // AtualizaKeys();
        }

        private static void CarregaDicionario()
        {
            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Dermatologia",
                Sinonimos = new List<string>( new string[]{ "DERMATOLOGISTA", "DERMATOLOGICO", "DERMATOLOGIA", "DERMATOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Psicologia",
                Sinonimos = new List<string>(new string[] { "PSICOLOGO", "PSICOLOGIA", "PSICO", "PSICOLOGICO", "PSICOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Cardiologia",
                Sinonimos = new List<string>(new string[] { "CARDIO", "CARDIOLOGISTA", "CARDIOLOGIA", "CARDIOVASCULAR", "VASCULAR", "CARDIOLOGICA", "CARDIOLOGICO" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Pneumologia",
                Sinonimos = new List<string>(new string[] { "PNEUMOLOGIA", "PNEUMOLOGISTA", "PNEUMO", "PNEUMOLOGICO", "PNEUMOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Endocrinologia",
                Sinonimos = new List<string>(new string[] { "ENDOCRINOLOGIA", "ENDOCRINOLOGISTA", "ENDOCRINO", "ENDOCRINOLOGICO", "ENDOCRINOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Fonoaudiologia",
                Sinonimos = new List<string>(new string[] { "FONOAUDIOLOGIA", "FONOAUDIOLOGISTA", "FONO", "FONOAUDIOLOGO", "FONOAUDIOLOGICO", "FONOAUDIOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Ginecologia",
                Sinonimos = new List<string>(new string[] { "GINECOLOGIA", "GINECOLOGISTA", "GINECOLOGICO", "GINECO", "GINECOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Obstetrícia",
                Sinonimos = new List<string>(new string[] { "OBSTETRICIA", "OBSTETRA", "OBSTETRO", "OBSTETROLOGIA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Oftalmologia",
                Sinonimos = new List<string>(new string[] { "OFTALMOLOGIA", "OFTALMOLOGISTA", "OFTALMO", "OFTALMOLOGICO", "OFTALMOLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Pediatria",
                Sinonimos = new List<string>(new string[] { "PEDIATRIA", "PEDIATRA", "PEDIATRICO", "PEDIATRICA"})
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Urologia",
                Sinonimos = new List<string>(new string[] { "UROLOGIA", "UROLOGISTA", "UROLOGICO", "UROLOGICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Gastroenterologia",
                Sinonimos = new List<string>(new string[] { "GASTROENTEROLOGIA", "GASTROLOGOLICO", "GASTROLOGOLISTA", "GASTROENTEROLOGISTA", "GASTRO", "GASTROENTEROLOGISTA", "GASTROLOGIA", "GASTROLOGOLICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Infectologia",
                Sinonimos = new List<string>(new string[] { "INFECTOLOGIA", "INFECTOLOGISTA", "INFECTO", "MICROBIOLOGICO", "MICROBIOLOGISTA", "INFECTOLOGICA", "INFECTOLOGICO"})
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Neurologia",
                Sinonimos = new List<string>(new string[] { "NEUROLOGIA", "NEURO", "NEUROLOGISTA", "NEUROLOGICO", "NEUROLOGICA"})
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Ortopedia",
                Sinonimos = new List<string>(new string[] { "ORTOPEDIA", "ORTOPEDISTA", "ORTOPEDICO", "ORTOPEDICA" })
            });

            Especializacoes.Add(new Model.Especialidades.Especialidade
            {
                Nome = "Reumatologia",
                Sinonimos = new List<string>(new string[] { "REUMATOLOGIA", "REUMATOLOGISTA", "REUMATOLOGICO", "REUMATOLOGICA" })
            });
        }

        private static void AtualizaPorEspecialidade(string especialidade, string AddKey)
        {
            UnidadeService service = new UnidadeService();
            Unidades = service.GetUnidadeByKey(AddKey, 100000);
            foreach (var unidade in Unidades)
            {
                var keys = new string[unidade.Keys.Length + 1];
                keys[0] = AddKey;
                unidade.Keys.CopyTo(keys, 1);
                unidade.Keys = keys;
                Console.WriteLine("Atualizando " + unidade.Name);
                service.Update(unidade);
            }

        }

        private static void AtualizaKeys()
        {
            UnidadeService service = new UnidadeService();
            foreach (var especializacao in Especializacoes)
            {
                foreach (var sinonimo in especializacao.Sinonimos)
                {
                    Unidades = service.GetUnidadeByKey(sinonimo, 100000);
                    foreach(var unidade in Unidades)
                    {                        
                        string[] novasKeys = especializacao.Sinonimos.ToArray();
                        var concatenadas = new string[unidade.Keys.Length + novasKeys.Length];
                        novasKeys.CopyTo(concatenadas, 0);
                        unidade.Keys.CopyTo(concatenadas, novasKeys.Length);
                        unidade.Keys = concatenadas;
                        Console.WriteLine("Atualizando " + unidade.Name);
                        service.Update(unidade);
                    }
                }

            }
        }

        private static void AtualizaDicionario()
        {
            UnidadeService service = new UnidadeService();
            foreach(var especializacao in Especializacoes)
            {
                foreach(var sinonimo in especializacao.Sinonimos)
                {
                    Unidades = service.GetUnidadeByKey(sinonimo, 100000);

                    foreach (var unidade in Unidades)
                    {
                        string result = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unidade.Name.ToLower());
                        unidade.Name = result;

                        if (unidade.Especialidades == null)
                        {
                            unidade.Especialidades = new List<string>();
                        }

                        if (unidade.Especialidades.Count == 0 || unidade.Especialidades.IndexOf(especializacao.Nome.ToUpper()) == 0)
                        {
                            unidade.Especialidades.Add(especializacao.Nome);
                        }

                        var nameKeys = CriaNomeKey(unidade);
                        int total = nameKeys.Length + especializacao.Sinonimos.Count;
                        string[] keys = new string[total];

                        for (int i = 0; i < nameKeys.Length; i++)
                        {
                            keys[i] = nameKeys[i];
                        }

                        for (int i = nameKeys.Length; i < especializacao.Sinonimos.Count + nameKeys.Length; i++)
                        {
                            keys[i] = especializacao.Sinonimos[i - nameKeys.Length];
                        }

                        unidade.Keys = keys;
                        Console.WriteLine("Atualizando " + unidade.Name);
                        service.Update(unidade);
                    }
                }

                
            }           
        }

        private static void GetCidades()
        {
            UnidadeService service = new UnidadeService();
            StreamReader reader = new StreamReader("D://Lista_cidades.csv");
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                try
                {
                    Model.Map.Municipio cidade = new Model.Map.Municipio();
                    cidade.UF = values[0];
                    cidade.Nome = values[1];
                    cidade.Lat = values[2];
                    cidade.Long = values[3];

                    Cidades.Add(cidade);
                }
                catch 
                {
                    Console.WriteLine("erro");
                }
            }
            UpdateMapa();
        }

        private static void UpdateMapa()
        {
            MunicipioService service = new MunicipioService();
            foreach (var cidade in Cidades)
            {
                Console.WriteLine("Gravando: " + cidade.Nome);
                service.Insert(cidade);
            }
        }

        private static void InsertOnDatabase()
        {
            SaveOnDatabase(Unidades);
        }

        private static void GetTelefonesFromFile()
        {
            UnidadeService service = new UnidadeService();
            StreamReader reader = new StreamReader("D://Q//id_telefones.csv");
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');

                try
                {
                    Model.Unidade unidade = new Unidade();
                    Int32 idbase = Convert.ToInt32(values[0]);

                    unidade = Unidades.Find(u => u.IdBase == idbase);
                    int index = Unidades.FindIndex(u => u.IdBase == idbase);

                    Telefone telefone = new Telefone();
                    telefone.DDD = values[1];
                    telefone.Numero = values[2];

                    unidade.Telefones = new List<Telefone>();
                    unidade.Telefones.Add(telefone);

                    Unidades[index] = unidade;

                    Console.WriteLine("Atualizando telefone: " + unidade.Name);

                }
                catch
                {
                    Console.WriteLine("Não gravou");
                }
            }
            reader.Close();
        }

        private static void GetEnderecosFromFile()
        {
            UnidadeService service = new UnidadeService();
            StreamReader reader = new StreamReader("D://Q//id_enderecos.csv");
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');

                try
                {
                    Model.Unidade unidade = new Unidade();
                    Int32 idbase = Convert.ToInt32(values[0]);

                    unidade = Unidades.Find(u => u.IdBase == idbase);
                    int index = Unidades.FindIndex(u => u.IdBase == idbase);

                    unidade.Endereco = new Endereco();

                    unidade.Endereco.Logradouro = values[1];
                    unidade.Endereco.Numero = values[2];
                    unidade.Endereco.Complemento = values[3];
                    unidade.Endereco.Bairro = values[4];
                    unidade.Endereco.Cidade = new Cidade();
                    unidade.Endereco.Cidade.Name = values[5];
                    unidade.Endereco.Cidade.Estado = new Estado();
                    unidade.Endereco.Cidade.Estado.UF = values[6];                    
                    unidade.Endereco.CEP = values[7];

                    //Atualiza geolocalização
                    if (unidade.Endereco != null)
                    {
                        if (unidade.Endereco.Cidade != null)
                        {
                            if (!String.IsNullOrEmpty(unidade.Endereco.Cidade.Name))
                            {
                                var locationService = new LocationService();
                                unidade.Endereco = locationService.GeocodeAddress(unidade.Endereco);
                                Console.WriteLine("Endereço atualizado: " + unidade.Name);


                                string tempName = "";
                                Console.WriteLine("Cidade: " + tempName);
                                tempName += " " + unidade.Endereco.Cidade.Name;
                                tempName = tempName.ToUpper();
                                tempName = tempName.Normalize();

                                byte[] tempBytes;
                                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(tempName);
                                string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

                                unidade.Endereco.Cidade.KeyName = asciiStr;
                                Console.WriteLine("Atualizando city keys: " + unidade.Endereco.Cidade.Name);

                                if (unidade.Name.Length < 3)
                                    unidade.Name = unidade.RazaoSocial;
                                if (unidade.Name.Contains("*"))
                                    unidade.Name = unidade.RazaoSocial;

                                tempName = unidade.Name;
                                Console.WriteLine("Unidade: " + tempName);
                                tempName += " " + unidade.Endereco.Cidade.Name;
                                tempName = tempName.Replace(" DE ", " ");
                                tempName = tempName.Replace(" E ", " ");
                                tempName = tempName.Replace(" DO ", " ");
                                tempName = tempName.Replace(" LTDA", "");
                                tempName = tempName.Replace(" LTDA.", "");
                                tempName = tempName.Replace(" LTDA ", "");
                                tempName = tempName.Replace("- ME.", "");
                                tempName = tempName.Replace("- ME.", "");
                                tempName = tempName.Replace("@", "");
                                tempName = tempName.Replace(",", "");
                                tempName = tempName.Replace(".", "");
                                tempName = tempName.Replace("- ME", "");
                                tempName = tempName.Replace(" - ", " ");
                                tempName = tempName.Replace("- ", " ");
                                tempName = tempName.Replace(" -", " ");
                                tempName = tempName.Replace(" DA ", " ");
                                tempName = tempName.Replace("\\", "");
                                tempName = tempName.Replace("  ", " ");

                                tempName = tempName.ToUpper();
                                tempName = tempName.Normalize();

                                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(tempName);
                                asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

                                var keys = asciiStr.Split(' ');
                                int total = keys.Length;
                                unidade.Keys = new string[total];
                                unidade.Keys = keys;
                            }
                        }
                    }//Fim geo

                    
                    //TESTAR DE ATUALIZA DADOS
                    Console.WriteLine("Atualizando endereço: " + unidade.Name);
                    Unidades[index] = unidade;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erro: " + ex.Message);
                }
            }
            reader.Close();
        }

        private static void GetEmpresasFromFile()
        {
            UnidadeService service = new UnidadeService();
            StreamReader reader = new StreamReader("D://Q//id_nomes.csv");
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');

                try
                {
                    Model.Unidade unidade = new Unidade();
                    unidade.IdBase = Convert.ToInt32(values[0]);
                    unidade.CNPJ = values[1];
                    unidade.RazaoSocial = values[2];                    
                    unidade.Name = values[3];
                    Console.WriteLine("Obtendo nome: " + unidade.Name);
                    Unidades.Add(unidade);
                }
                catch
                {
                    Console.WriteLine("Não gravou");
                }
            }
            reader.Close();
        }

        private static void CriaCityKeys()
        {
            UnidadeService service = new UnidadeService();
            var unidades = service.GetUnidadesByCEP("04103-000");
            foreach (var unidade in unidades)
            {
                if (string.IsNullOrEmpty(unidade.Endereco.Cidade?.Name))
                {
                    service.Delete(unidade);
                }
                else
                {
                    string tempName = "";
                    Console.WriteLine("Cidade: " + tempName);
                    tempName += " " + unidade.Endereco.Cidade.Name;
                    tempName = tempName.ToUpper();
                    tempName = tempName.Normalize();

                    byte[] tempBytes;
                    tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(tempName);
                    string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

                    unidade.Endereco.Cidade.KeyName = asciiStr;
                    Console.WriteLine("Atualizando city keys: " + unidade.Endereco.Cidade.Name);
                    service.Update(unidade);
                }
            }
            Console.WriteLine("Todos os registros atualizados");
            Console.ReadLine();
        }

        private static void CriaKeys()
        {
            UnidadeService service = new UnidadeService();
            var unidades = service.GetUnidadesByCEP("04103-000");
            foreach (var unidade in unidades)
            {
                if (string.IsNullOrEmpty(unidade.Endereco.Cidade?.Name))
                {
                    service.Delete(unidade);
                }
                else
                {
                    var keys = CriaNomeKey(unidade);
                    int total = keys.Length;
                    unidade.Keys = new string[total];
                    unidade.Keys = keys;
                    Console.WriteLine("Atualizando keys: " + unidade.Name);
                    service.Update(unidade);
                }
            }
            Console.ReadLine();
        }

        private static string[] CriaNomeKey(Unidade unidade)
        {
            if (unidade.Name.Length < 3)
                unidade.Name = unidade.RazaoSocial;
            if (unidade.Name.Contains("*"))
                unidade.Name = unidade.RazaoSocial;
            string tempName = unidade.Name;
            Console.WriteLine("Unidade: " + tempName);
            tempName += " " + unidade.Endereco.Cidade.Name;
            tempName += " " + unidade.Endereco.Cidade.Estado.UF;
            tempName += " " + unidade.Endereco.CEP;
            tempName = tempName.Replace(" DE ", " ");
            tempName = tempName.Replace(" E ", " ");
            tempName = tempName.Replace(" DO ", " ");
            tempName = tempName.Replace(" LTDA", "");
            tempName = tempName.Replace(" LTDA.", "");
            tempName = tempName.Replace(" LTDA ", "");
            tempName = tempName.Replace("- ME.", "");
            tempName = tempName.Replace("- ME.", "");
            tempName = tempName.Replace("@", "");
            tempName = tempName.Replace(",", "");
            tempName = tempName.Replace(".", "");
            tempName = tempName.Replace("- ME", "");
            tempName = tempName.Replace(" - ", " ");
            tempName = tempName.Replace("- ", " ");
            tempName = tempName.Replace(" -", " ");
            tempName = tempName.Replace(" DA ", " ");
            tempName = tempName.Replace("\\", "");
            tempName = tempName.Replace("  ", " ");

            tempName = tempName.ToUpper();
            tempName = tempName.Normalize();

            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(tempName);
            string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

            var keys = asciiStr.Split(' ');
            return keys;
        }

        private static void CriaUnidade()
        {
            UnidadeService service = new UnidadeService();
            var unidade = new Unidade();
            unidade.Name = "Hospital e Maternidade Santa Joana";
            unidade.RazaoSocial = "Hospital e Maternidade Santa Joana SA";
            unidade.CNPJ = "60678604000113";

            unidade.TiposEstabelecimento = new TipoEstabelecimento();
            unidade.TiposEstabelecimento.Descricao = "ATIVIDADES DE ATENDIMENTO HOSPITALAR, EXCETO PRONTO-SOCORRO E UNIDADES PARA ATENDIMENTO A URGÊNCIAS";

            unidade.Endereco = new Endereco();
            unidade.Endereco.Logradouro = "Rua Paraíso";
            unidade.Endereco.Numero = "432";
            unidade.Endereco.Bairro = "Liberdade";
            unidade.Endereco.CEP = "04103-000";
            unidade.Endereco.Cidade = new Cidade();
            unidade.Endereco.Cidade.Name = "São Paulo";
            unidade.Endereco.Cidade.Estado = new Estado();
            unidade.Endereco.Cidade.Estado.UF = "SP";

            var locationService = new LocationService();
            unidade.Endereco = locationService.GeocodeAddress(unidade.Endereco);

            unidade.Telefones = new List<Telefone>();
            unidade.Telefones.Add(new Telefone { DDD = "11", Numero = "5080-6000" });
            service.Insert(unidade);
        }

        private static void CorrigeEndereco()
        {
            UnidadeService service = new UnidadeService();
            var unidade = service.GetUnidadeByName("Hospital e Maternidade Santa Joana");
            unidade.Endereco = new Endereco();
            unidade.Endereco.Logradouro = "Rua Paraíso";
            unidade.Endereco.Numero = "432";
            unidade.Endereco.Bairro = "Liberdade";
            unidade.Endereco.CEP = "04103-000";
            unidade.Endereco.Cidade = new Cidade();
            unidade.Endereco.Cidade.Name = "São Paulo";
            unidade.Endereco.Cidade.Estado = new Estado();
            unidade.Endereco.Cidade.Estado.UF = "SP";

            var locationService = new LocationService();
            unidade.Endereco = locationService.GeocodeAddress(unidade.Endereco);

            unidade.Telefones = new List<Telefone>();
            unidade.Telefones.Add(new Telefone { DDD = "11", Numero = "5080-6000" });
            service.Update(unidade);
        }

        private static void LimpaEnderecos()
        {
            UnidadeService service = new UnidadeService();
            var unidades = service.GetUnidadesByCEP("");
            foreach (var unidade in unidades)
            {              
                unidade.Endereco.Complemento = null;
                service.Update(unidade);
            }
        }

        private static void SaveOnDatabase(List<Unidade> unidades)
        {
            UnidadeService service = new UnidadeService();
            foreach(var unidade in unidades)
            {
                Console.WriteLine("Gravando: " + unidade.Name);
                service.Insert(unidade);
            }   
        }

        private static void GetUnidadeFromFile()
        {
            UnidadeService service = new UnidadeService();
            StreamReader reader = new StreamReader("D://Unidades.csv");
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');

                try
                {
                    Model.Unidade unidade = new Unidade();
                    unidade.CNPJ = values[0];
                    unidade.Name = values[1];
                    unidade.RazaoSocial = values[2];
                    unidade.TiposEstabelecimento = new TipoEstabelecimento();
                    unidade.TiposEstabelecimento.Descricao = values[3];
                    unidade.Telefones = new List<Telefone>();
                    unidade.Telefones.Add(new Telefone { DDD = values[4], Numero = values[5] });
                    unidade.Endereco = new Endereco();
                    unidade.Endereco.CEP = values[6];
                    unidade.Endereco.Logradouro = values[7] + " " + values[8];
                    unidade.Endereco.Bairro = values[9];
                    unidade.Endereco.Cidade = new Cidade();
                    unidade.Endereco.Cidade.Name = values[10];
                    unidade.Endereco.Cidade.Estado = new Estado();
                    unidade.Endereco.Cidade.Estado.UF = values[11];
                    unidade.Endereco.Numero = values[12];
                    unidade.Endereco.Complemento = values[13];
                    Console.WriteLine("Obtendo: " + unidade.Name);

                

                //Atualiza geolocalização
                if (unidade.Endereco != null)
                {
                    if (unidade.Endereco.Cidade != null)
                    {
                        if (!String.IsNullOrEmpty(unidade.Endereco.Cidade.Name))
                        {
                            var locationService = new LocationService();
                            unidade.Endereco = locationService.GeocodeAddress(unidade.Endereco);
                            Console.WriteLine("Endereço atualizado: " + unidade.Name);


                            string tempName = "";
                            Console.WriteLine("Cidade: " + tempName);
                            tempName += " " + unidade.Endereco.Cidade.Name;
                            tempName = tempName.ToUpper();
                            tempName = tempName.Normalize();

                            byte[] tempBytes;
                            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(tempName);
                            string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

                            unidade.Endereco.Cidade.KeyName = asciiStr;
                            Console.WriteLine("Atualizando city keys: " + unidade.Endereco.Cidade.Name);

                            if (unidade.Name.Length < 3)
                                unidade.Name = unidade.RazaoSocial;
                            if (unidade.Name.Contains("*"))
                                unidade.Name = unidade.RazaoSocial;

                            tempName = unidade.Name;
                            Console.WriteLine("Unidade: " + tempName);
                            tempName += " " + unidade.Endereco.Cidade.Name;
                            tempName = tempName.Replace(" DE ", " ");
                            tempName = tempName.Replace(" E ", " ");
                            tempName = tempName.Replace(" DO ", " ");
                            tempName = tempName.Replace(" LTDA", "");
                            tempName = tempName.Replace(" LTDA.", "");
                            tempName = tempName.Replace(" LTDA ", "");
                            tempName = tempName.Replace("- ME.", "");
                            tempName = tempName.Replace("- ME.", "");
                            tempName = tempName.Replace("@", "");
                            tempName = tempName.Replace(",", "");
                            tempName = tempName.Replace(".", "");
                            tempName = tempName.Replace("- ME", "");
                            tempName = tempName.Replace(" - ", " ");
                            tempName = tempName.Replace("- ", " ");
                            tempName = tempName.Replace(" -", " ");
                            tempName = tempName.Replace(" DA ", " ");
                            tempName = tempName.Replace("\\", "");
                            tempName = tempName.Replace("  ", " ");

                            tempName = tempName.ToUpper();
                            tempName = tempName.Normalize();

                            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(tempName);
                            asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

                            var keys = asciiStr.Split(' ');
                            int total = keys.Length;
                            unidade.Keys = new string[total];
                            unidade.Keys = keys;
                        }
                    }
                }
                    service.Insert(unidade); ;
                }
                catch
                {
                    Console.WriteLine("Não gravou");
                }
            }
            
            reader.Close();
            
        }

        public static void UpdateNotas()
        {
            UnidadeService service = new UnidadeService();
            var unidades = service.GetUltimasAvaliacoes();
            foreach (var unidade in unidades)
            {
                foreach (var avalia in unidade.Avaliacoes)
                {
                    if (avalia.Estrutura)
                        avalia.NotaEstrutura = 80;
                    else
                    {
                        avalia.NotaEstrutura = 20;
                    }
                    if (avalia.Atendimento)
                        avalia.NotaAtendimento = 80;
                    else
                    {
                        avalia.NotaAtendimento = 20;
                    }
                    if (avalia.HigieneLimpeza)
                        avalia.NotaHigieneLimpeza = 80;
                    else
                    {
                        avalia.NotaHigieneLimpeza = 20;
                    }
                    if (avalia.Recomenda)
                        avalia.NotaRecomenda = 80;
                    else
                    {
                        avalia.NotaRecomenda = 20;
                    }
                }
                service.Update(unidade);
            }

        }

        public static void GetInMS()
        {
            var w = new WebClient();
            string s = w.DownloadString("http://cnes2.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=12&NomeEstado=ACRE");
            foreach (LinkItem i in LinkFinder.Find(s))
            {
                var client = new WebClient();
                string l = client.DownloadString("http://cnes2.datasus.gov.br/" + i.Href);
                foreach (LinkItem j in LinkFinder.Find(l))
                {
                    var clientDetail = new WebClient();
                    string data = "";
                    try
                    {
                        data = clientDetail.DownloadString("http://cnes2.datasus.gov.br/" + j.Href);
                    }
                    catch
                    {
                        data = clientDetail.DownloadString("http://cnes2.datasus.gov.br/" + j.Href);
                    }

                    string nomeUnidade = GetHtmlPage(data);
                    Console.WriteLine("Obtendo dados de: " + nomeUnidade);
                }

            }
            Console.WriteLine("Pesquisa de unidades concluída");
            Console.ReadLine();
        }

        public static string GetHtmlPage(string strResult)
        {
            if (!strResult.Contains("Pagina de Manutenção")) { 
                string[] values = strResult.Split(new string[] { "<table>", "</table>" }, StringSplitOptions.RemoveEmptyEntries);
                return HtmlToUnidade("<tbody>" + values[3] + "</tbody>").Name;

            }
            return "Página fora do ar";
        }

        private static Unidade HtmlToUnidade(string HTML)
        {
            string[] linhas = HTML.Split(new string[] { "<font size=1 face=Verdana,arial color=#003366>", "</font>" }, StringSplitOptions.RemoveEmptyEntries);

            Model.Unidade unidade = new Unidade();
            unidade.Name = linhas[7];
            string cnes = linhas[8];
            unidade.CNES = cnes;
            unidade.CNPJ = linhas[11];

            unidade.RazaoSocial = linhas[19];
            unidade.Endereco = new Endereco();
            unidade.Endereco.Logradouro = linhas[31];
            unidade.Endereco.Numero = linhas[33];
            unidade.Endereco.Complemento = linhas[45];
            unidade.Endereco.Bairro = linhas[47];
            unidade.Endereco.CEP = linhas[49];
            unidade.Endereco.Cidade = new Cidade();
            string[] cidade = linhas[50].Split(new string[] { "<font color='#003366' face=verdana,arial size=1>", "</font>"}, StringSplitOptions.RemoveEmptyEntries);            
            if (cidade.Length == 1)
            {
                cidade = linhas[51].Split(new string[] { "<font color='#003366' face=verdana,arial size=1>", "</font>" }, StringSplitOptions.RemoveEmptyEntries);
            }

            if (cidade.Length == 2) 
            { 
                unidade.Endereco.Cidade.Name = cidade[1];
            }
            unidade.Endereco.Cidade.Estado = new Estado();
            unidade.Endereco.Cidade.Estado.UF = linhas[53];
            unidade.TiposEstabelecimento = new TipoEstabelecimento {Descricao = linhas[63]};
            unidade.EsferaAdministrativa = new EsferaAdministrativa { Descricao = linhas[66] };
            unidade.Gestao = new Gestao {Descricao = linhas[68] };

            unidade.Telefones = new List<Telefone>();
            unidade.Telefones.Add(new Telefone{ Numero = linhas[35] });

            UnidadeService service = new UnidadeService();
            service.Insert(unidade);

            return unidade;
        }
    }
}
 
 