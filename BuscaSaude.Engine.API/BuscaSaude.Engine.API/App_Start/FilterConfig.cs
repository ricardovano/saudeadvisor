﻿using System.Web;
using System.Web.Mvc;

namespace BuscaSaude.Engine.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
