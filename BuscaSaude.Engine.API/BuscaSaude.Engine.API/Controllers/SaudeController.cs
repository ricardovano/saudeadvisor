﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.API.Controllers
{
    public class SaudeController : ApiController
    {
       
        [HttpGet]
        public HttpResponseMessage GetUnidadeByName(string name)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetUnidadeByName(name));
        }

        [HttpGet]
        public HttpResponseMessage GetUnidadeBySearch(string searchString)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetUnidadeBySearchString(searchString));
        }

        [HttpGet]
        public HttpResponseMessage GetUnidadeById(string unidadeId)
        {
            var unidadesService = new UnidadeService();
            var unidade = unidadesService.GetUnidadeById(unidadeId);

            if ((unidade.Endereco.CEP != null) && unidade.Endereco.CEP.Contains("<td>"))
                unidade.Endereco.CEP = null;

            if ((unidade.Endereco.Bairro != null) && unidade.Endereco.Bairro.Contains("<td>"))
                unidade.Endereco.Bairro = null;

            if (unidade.Endereco.Location == null || ((unidade.Endereco.Cidade.Name != null) && unidade.Endereco.Cidade.Name.Contains("IBGE")))
            {                            
                var locationService = new LocationService();
                unidade.Endereco = locationService.GeocodeAddress(unidade.Endereco);
                unidadesService.Update(unidade);
            }            
            return Request.CreateResponse(HttpStatusCode.OK, unidade);
        }

        [HttpGet]
        public HttpResponseMessage GetEstatisticaByUnidadeId(string estatisticaUnidadeId)
        {
            var unidadesService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadesService.GetEstatisticaByUnidadeId(estatisticaUnidadeId));
        }

        [HttpPost]
        public HttpResponseMessage SetAvaliacaoByUnidadeId(AvaliacaoDTO avaliacao)
        {
            var unidadesService = new UnidadeService();
            unidadesService.SetAvaliacaoByUnidadeId(avaliacao);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage GetUnidadeByKey(string nameKey)
        {
            nameKey = nameKey.ToUpper();

            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(nameKey);
            string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

            var unidadesService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadesService.GetUnidadeByKey(asciiStr));
        }
    }
}
