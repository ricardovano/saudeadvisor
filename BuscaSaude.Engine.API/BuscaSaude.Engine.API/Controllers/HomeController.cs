﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model;

namespace BuscaSaude.Engine.API.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


    }
}