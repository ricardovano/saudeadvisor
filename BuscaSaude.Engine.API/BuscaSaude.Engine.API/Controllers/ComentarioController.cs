﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.API.Controllers
{
    public class ComentarioController : ApiController
    {
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetLastComentariosByRegion(string region)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetLastComentariosByRegion(region));
        }
    }
}