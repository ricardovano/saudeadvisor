﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model;

namespace BuscaSaude.Engine.API.Controllers
{
    public class SiteController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage SendContact(Contact contact)
        {
            var unidadesService = new UnidadeService();
            unidadesService.SendContact(contact);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage GetMensagens(bool incluirLidos)
        {
            var unidadesService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadesService.GetMensagens(incluirLidos));
        }

        [HttpGet]
        public HttpResponseMessage GetLastAvaliacoes()
        {
            var unidadesService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadesService.GetUltimasAvaliacoes());
        }
    }
}