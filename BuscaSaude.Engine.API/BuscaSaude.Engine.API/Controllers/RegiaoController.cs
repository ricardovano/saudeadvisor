﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.API.Controllers
{
    public class RegiaoController : ApiController
    {
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetRegionByName(RegionDTO region)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetRegionByName(region));
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetRegionByName(string name)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetRegionByName(name));
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetTotal(string region)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetTotalByRegion(region));
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetMunicipios()
        {
            var municipioService = new MunicipioService();
            return Request.CreateResponse(HttpStatusCode.OK, municipioService.GetAll());
        }
    }
}