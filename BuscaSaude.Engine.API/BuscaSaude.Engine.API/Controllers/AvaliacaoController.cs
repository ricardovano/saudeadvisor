﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BuscaSaude.Engine.Core;
using BuscaSaude.Engine.Model.DTO;

namespace BuscaSaude.Engine.API.Controllers
{
    public class AvaliacaoController : ApiController
    {
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetBestByRegion(string region)
        {
            var unidadeService = new UnidadeService();
            return Request.CreateResponse(HttpStatusCode.OK, unidadeService.GetBestByRegion(region));
        }
    }
}