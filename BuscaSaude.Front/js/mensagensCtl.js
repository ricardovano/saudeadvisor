app.controller('mensagensCtl', function ($scope, $http) {
    var s = gup('s');

    if (s == 'r') {
        $scope.lidos = true;
        $scope.mensagens = [];
        $scope.loading = true;

        $http.get(getUrl('Site') + '/GetMensagens?incluirLidos=' + $scope.lidos)
            .success(function (data) {
                $scope.mensagens = data;
                $scope.loading = false;
            })
            .error(function (data) {
                console.log('Erro: ' + data);
                $scope.loading = false;
            });
    }
});