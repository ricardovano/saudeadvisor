app.controller('regiaoCtl', function ($scope, $http, $cookies) {
    $scope.cidade = $cookies.cidade;
    $scope.melhores = [];
    $scope.total = 0;
    $scope.limit = 100;
    $scope.count = 0;
    $scope.region = { region: $scope.cidade, limit: $scope.limit,  skip: $scope.total};
    $scope.unidades = [];

    $scope.getDetail = function(name){
        return name.replace(/ /g, '-');
    }

    $scope.getColor = function(index){
        if(index % 2 == 0)
            return '#f3f3f3';
        return '#fff';
    }

    $scope.loadingBest = true;
    $http.get(getUrl('Avaliacao') + '/GetBestByRegion?region=' + $scope.cidade )
        .success(function(data) {
            $scope.melhores = data;
            $scope.loadingBest = false;
        })
        .error(function(data) {
            console.log('Erro: ' + data);
            $scope.loading = false;
        }
    )

    $scope.loadingComments = true;
    $http.get(getUrl('Comentario') + '/GetLastComentariosByRegion?region=' + $scope.cidade )
        .success(function(data) {
            $scope.Avaliacoes = data;
            $scope.loadingComments = false;
        })
        .error(function(data) {
            console.log('Erro: ' + data);
            $scope.loading = false;
        }
    )

    $scope.loading = true;
    var getUnidades = function(){
        $http({
            dataType: "json",
            method: 'POST',
            url: getUrl('Regiao') + '/GetRegionByName',
            data: $scope.region,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
            .success(function (data) {

                $scope.unidades =$scope.unidades.concat(data);
                $scope.region.skip = $scope.region.skip + $scope.limit;

                if($scope.count > $scope.region.skip)
                    getUnidades();

                $scope.loading = false;
            })
            .error(function (data) {
                console.log('Erro: ' + data);
                $scope.loading = false;
            }
        );
    }

    $http.get(getUrl('Regiao') + '/GetTotal?region=' + $scope.cidade )
        .success(function(data) {
            $scope.count = data;
            $scope.loading = false;
            getUnidades();
        })
        .error(function(data) {
            console.log('Erro: ' + data);
            $scope.loading = false;
        }
    )

});
