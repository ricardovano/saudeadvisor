app.controller('footerCtl', function ($scope, $http) {
    $scope.messageContact = "";
    $scope.sentContact = false;
    $scope.Contact = {Name: '', Email: '', Descricao:''};
    $scope.sendContact = function(){
        if ($scope.Contact.Email.length <= 0){
            $scope.messageContact = "Ops! Insira seu email.";
            return;
        }

        if ($scope.Contact.Email.indexOf('@') < 1 || $scope.Contact.Email.indexOf('.') < 1){
            $scope.messageContact = "Xii! Esse email que você inseriu não existe!";
            return;
        }

        if ($scope.Contact.Name.length <= 0){
            $scope.messageContact = "Ops! Diga-nos qual é o seu nome.";
            return;
        }

        if ($scope.Contact.Descricao.length <= 0){
            $scope.messageContact = "Ops! Insira um comentário. Conte pra gente...";
            return;
        }

        $scope.messageContact = "";
        $http({
            dataType: "json",
            method: 'POST',
            url: getUrl('Site') + '/SendContact',
            data: $scope.Contact,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
            .success(function (data) {
                $scope.sentContact = true;
                $scope.agradeceContact = $scope.Contact.Name;
            })
            .error(function (data) {
                console.log('Erro: ' + data);
            });
    }
});