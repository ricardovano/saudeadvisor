﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BuscaSaude.Engine.Model;
using BuscaSaude.Engine.Model.DTO;
using System.IO;
using BuscaSaude.Engine.Core;

namespace BuscaSaude.Front.Controllers
{
    public class SaudeController : Controller
    {
        public static string URLSaude = "http://localhost:8090/api/Saude/";
        public static string URLRegiao = "http://localhost:8090/api/Regiao/";
        public static string URLSite = "http://localhost:8090/api/Site/";

        //Localhost address
        public static string GoogleOAuth = "878347470886-e4tpeu8emosipih61ajp1r7hqccmvj05.apps.googleusercontent.com";
        public static string GoogleSecretKey = "3WUoqNfE9lXlil4oELpbeC_e";

        public UnidadeService unidadesService;
        public SaudeController()
        {
            unidadesService = new UnidadeService();
        }

        [RequireHttps]
        public ActionResult Index()
        {
            ViewBag.Title = "SaudeAdvisor - Consulte centros médicos, clínicas e laboratórios";
            return View();
        }

        public ActionResult Privacidade()
        {
            ViewBag.Title = "SaudeAdvisor - Política de Privacidade";
            return View();
        }

        public ActionResult Sobre()
        {
            ViewBag.Title = "Sobre SaudeAdvisor";
            return View();
        }


        public ActionResult FaleConosco()
        {
            ViewBag.Title = "Entre em contato com SaudeAdvisor";
            ViewBag.KeyWords = ", SaudeAdvisor";
            return View();
        }

        [HttpPost]
        public ActionResult FaleConosco(BuscaSaude.Engine.Model.Contact contato)
        {
            ViewBag.Title = "SaudeAdvisor";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(URLSite + "SendContact"));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(contato);
            string parsedContent = json;
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            ViewBag.Enviado = true;
            return View();
        }

        public ActionResult Contato()
        {
            ViewBag.Title = "SaudeAdvisor - Entre em contato";
            return View();
        }

        [HttpPost]
        public ActionResult Contato(BuscaSaude.Engine.Model.Contact contato)
        {
            ViewBag.Title = "SaudeAdvisor - Contato";
            var http = (HttpWebRequest)WebRequest.Create(new Uri(URLSite + "SendContact"));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(contato);
            string parsedContent = json;
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            ViewBag.Enviado = true;
            return View();
        }

        public ActionResult Unidades(string pesquisa)
        {
            ViewBag.Title = pesquisa + " em SaudeAdvisor";
            if (!String.IsNullOrEmpty(pesquisa))
            { 
                ViewBag.Pesquisa = pesquisa;
                pesquisa = pesquisa.ToUpper();
                byte[] tempBytes;
                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(pesquisa);
                string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);                                                
                var unidades = unidadesService.GetUnidadeByKey(asciiStr);        
                
                if (unidades.Count < 1)
                {
                    ViewBag.Pesquisa = "Nao existem resultados para a pesquisa " + pesquisa;
                    return View();
                }                    
                return View(unidades);
            }
            return View();
        }

        public ActionResult Unidade(string pesquisa)
        {            
            if (!String.IsNullOrEmpty(pesquisa))
            {
                var unidade = unidadesService.GetUnidadeById(pesquisa);

                if ((unidade.Endereco.CEP != null) && unidade.Endereco.CEP.Contains("<td>"))
                    unidade.Endereco.CEP = null;

                if ((unidade.Endereco.Bairro != null) && unidade.Endereco.Bairro.Contains("<td>"))
                    unidade.Endereco.Bairro = null;

                if (unidade.Endereco.Location == null || ((unidade.Endereco.Cidade.Name != null) && unidade.Endereco.Cidade.Name.Contains("IBGE")))
                {
                    var locationService = new LocationService();
                    unidade.Endereco = locationService.GeocodeAddress(unidade.Endereco);
                    unidadesService.Update(unidade);
                }

                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
                ViewBag.Pesquisa = pesquisa;

                var pontuacao = unidadesService.GetEstatisticaByUnidadeId(unidade.Id);
                pontuacao.PontuacaoMedia = Convert.ToInt32(pontuacao.PontuacaoMedia);
                ViewBag.pontuacao = pontuacao;

                //string AppId = "WV1ejBrcW9Im7ym1Djii";
                //string AppCode = "hV9Ermv8lTXCi_bDRmWlZA";
                ViewBag.CoordX = unidade.Endereco.Location.Coordinates[1].ToString().Replace(",",".");
                ViewBag.CoordY = unidade.Endereco.Location.Coordinates[0].ToString().Replace(",", ".");
                ViewBag.Title = unidade.Name + " em " + unidade.Endereco.Cidade.Name;

                if (unidade.Telefones == null)
                {
                    unidade.Telefones = new List<Telefone>();
                    unidade.Telefones.Add(new Telefone { DDD = "", Numero = "" });
                }
                if (unidade.Endereco == null)
                {
                    unidade.Endereco = new Endereco();
                }

                return View(unidade);
            }

            return View();
 
        }

        public ActionResult Cidade(string pesquisa)
        {
            ViewBag.Pesquisa = pesquisa;
            ViewBag.Title = "Centros médicos, laboratórios, hospitais, clínicas em " + pesquisa;
            pesquisa = pesquisa.ToUpper();

            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(pesquisa);
            string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);

            string url = URLRegiao + "GetRegiaoByName?name=" + asciiStr;
            var client = new WebClient();
            client.Encoding = Encoding.UTF8;
            var content = client.DownloadString(url);

            JavaScriptSerializer ser = new JavaScriptSerializer();
            ser.MaxJsonLength = Int32.MaxValue;
            List<Unidade> unidades = ser.Deserialize<List<Unidade>>(content);
            
            var comentarios = new List<ComentarioDTO>();
            foreach (var unidade in unidades)
            {
                if (unidade.Avaliacoes == null)
                    unidade.Avaliacoes = new List<Avaliacao>();
                else
                {
                    foreach (var a in unidade.Avaliacoes)
                    {
                        ComentarioDTO c = new ComentarioDTO();
                        c.Comentario = a.Descricao;
                        c.Data = a.Data;
                        c.Nome = a.Name;
                        c.NotaRecomenda = a.NotaRecomenda;
                        c.Anonimo = a.Anonimo;
                        c.Unidade = unidade.Name;
                        c.UnidadeId = unidade.Id;
                        comentarios.Add(c);
                    }
                }

            }

            Engine.Model.DTO.CidadeDTO cidade = new Engine.Model.DTO.CidadeDTO();
            cidade.Unidades = new List<Unidade>();
            cidade.MelhoresUnidades = new List<Engine.Model.Unidade>();
            if (unidades.Count > 3)
                cidade.MelhoresUnidades = unidades.OrderByDescending(m => m.Score).ToList().GetRange(0, 3);

            cidade.Unidades = unidades;
            cidade.Comentarios = comentarios;

            return View(cidade);
        }

        public ActionResult AvaliaUnidade()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AvaliaUnidade(AvaliacaoDTO avalia)
        {
            return View();
        }
    }

    public class Regiao
    {
        public String City;
        public String UF;
        public List<Unidade> Unidades;
        public List<Unidade> BestList;
        public List<ComentarioDTO> CommentsList;
    }

    public class ClientPontuacao
    {
        public int Infraestrutura { get; set; }
        public int Atendimento { get; set; }
        public int LimpezaHigiene { get; set; }
        public int Recomendacoes { get; set; }
        public int PontuacaoMedia { get; set; }

        public string notasHLA { get; set; }
        public string notasHLB { get; set; }
        public string notasHLC { get; set; }
        public string notasHLD { get; set; }
        public string notasHLE { get; set; }

        public string notasATA { get; set; }
        public string notasATB { get; set; }
        public string notasATC { get; set; }
        public string notasATD { get; set; }
        public string notasATE { get; set; }

        public string notasESA { get; set; }
        public string notasESB { get; set; }
        public string notasESC { get; set; }
        public string notasESD { get; set; }
        public string notasESE { get; set; }

        public string notasOGA { get; set; }
        public string notasOGB { get; set; }
        public string notasOGC { get; set; }
        public string notasOGD { get; set; }
        public string notasOGE { get; set; }

        public string MediaStarA { get; set; }
        public string MediaStarB { get; set; }
        public string MediaStarC { get; set; }
        public string MediaStarD { get; set; }
        public string MediaStarE { get; set; }
    }
}