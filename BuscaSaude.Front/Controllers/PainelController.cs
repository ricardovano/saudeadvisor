﻿using System.Web.Mvc;
using System;
using System.Net;
using System.Text;
using System.IO;
using System.Web.Security;
using System.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Security.Claims;
using RestSharp.Extensions.MonoHttp;
using System.Globalization;
using System.IdentityModel.Services;

namespace BuscaSaude.Front.Controllers
{
    public class PainelController : Controller
    {
        Engine.Core.PainelService painel;
        public PainelController()
        {
            painel = new Engine.Core.PainelService();
        }

        [System.Web.Http.Authorize]
        public ActionResult Index(string code)
        {

            string uri = "https://saudeadvisor.auth0.com/oauth/token" +
                 "client_id=RtqHJ2uKyks3gAm4idQ6lD6WxMTzNjK8&redirect_uri=https://localhost:44363/Painel/" +
                 "&client_secret=1mJYTMhajTSk_XBm_mEoI4_DIodz4fAIrcKjHJRCbHcWOMTFrePlCnTdZsj5CIVr" +
                 "&code="+ code + "&grant_type=authorization_code";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(uri));
            http.ContentType = "Content-type: application/x-www-form-urlencoded";
            http.Method = "POST";
            ASCIIEncoding encoding = new ASCIIEncoding();
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            //string accessToken = content.access_token;
            string uriToken = "https://saudeadvisor.auth0.com/userinfo/?access_token=";
            var httpUser = (HttpWebRequest)WebRequest.Create(new Uri(uriToken));


            //var auth = User.Identity.IsAuthenticated;
            string email = ClaimsPrincipal.Current.FindFirst("email").Value;
            //string accessToken = ClaimsPrincipal.Current.FindFirst("access_token").Value;
            return View();
        }

        public ActionResult Login(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !this.Url.IsLocalUrl(returnUrl))
            {
                returnUrl = "/";
            }

            // you can use this for the 'authParams.state' parameter
            // in Lock, to provide a return URL after the authentication flow.
            ViewBag.State = "ru=" + HttpUtility.UrlEncode(returnUrl);

            return this.View();
        }


        public ActionResult LogOff()
        {
            // Clear the session cookie
            FederatedAuthentication.SessionAuthenticationModule.SignOut();

            // Redirect to Auth0's logout endpoint
            var returnTo = Url.Action("Index", "Saude", null, protocol: Request.Url.Scheme);
            return this.Redirect(
                string.Format(CultureInfo.InvariantCulture,
                    "https://{0}/v2/logout?returnTo={1}",
                    ConfigurationManager.AppSettings["auth0:Domain"],
                    this.Server.UrlEncode(returnTo)));
        }
    }
}